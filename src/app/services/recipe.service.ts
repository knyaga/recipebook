import { EventEmitter, Injectable, Output } from '@angular/core';

import { Recipe } from './../recipes/recipe.model';
import { Ingredient } from './../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';

@Injectable()
export class RecipeService {

    @Output() recipeSelected: EventEmitter<Recipe> = new EventEmitter<Recipe>();
    @Output() recipesChanged: EventEmitter<Recipe[]> = new EventEmitter<Recipe[]>();

    constructor(private shoppingListService: ShoppingListService) {}

    private recipes: Recipe[] = [
        new Recipe(
            'Pilau',
            'Very tasty and yummy food',
            'http://bigeye.ug/wp-content/uploads/2015/09/Pilau.png',
            [
                new Ingredient('Rice', 3),
                new Ingredient('Onion', 6),
                new Ingredient('Meat', 5),
            ]
        ),
        new Recipe(
            'Chapati',
            'Very tasty and yummy food',
            'http://simpleindianrecipes.com/portals/0/sirimages/Chapati-M.jpg',
            [
                new Ingredient('Flour', 4),
                new Ingredient('Oil', 1),
            ]
        )
    ];

    getRecipes() {
        return this.recipes.slice();
    }

    addToShoppingList(ingredients: Ingredient[]) {
      this.shoppingListService.addIngredients(ingredients);
    }

    getRecipeById(id: number) {
        return this.recipes[id];
    }

    addNewRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.emit(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.emit(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.emit(this.recipes.slice());
    }
}
