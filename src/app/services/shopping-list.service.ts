import { EventEmitter, Output } from '@angular/core';
import { Ingredient } from './../shared/ingredient.model';

export class ShoppingListService {

    @Output() startEditing: EventEmitter<number> = new EventEmitter<number>();
    @Output() ingredientsChanged: EventEmitter<Ingredient[]> = new EventEmitter<Ingredient[]>();

    private ingredients: Ingredient[] = [
        new Ingredient('Tomato', 4),
        new Ingredient('Rice', 5),
        new Ingredient('Meat', 3),
        new Ingredient('Onions', 2),
    ];

    getIngredients() {
        return this.ingredients.slice();
    }

    getIngredientById(index: number) {
        return this.ingredients[index];
    }

    updateIngredient(index: number, newIngredient: Ingredient) {
        this.ingredients[index] = newIngredient;
        this.ingredientsChanged.emit(this.ingredients.slice());
    }

    removeIngredient(index: number) {
        this.ingredients.splice(index, 1);
        this.ingredientsChanged.emit(this.ingredients.slice());
    }

    addIngredient(ingredient: Ingredient) {
        this.ingredients.push(ingredient);
        this.ingredientsChanged.emit(this.ingredients.slice());
    }

    addIngredients(ingredients: Ingredient[]) {
        this.ingredients.push(...ingredients);
        this.ingredientsChanged.emit(this.ingredients.slice());
    }
}