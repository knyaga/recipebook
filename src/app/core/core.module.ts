
import { NgModule } from '@angular/core';

import { HeaderComponent } from './header/header.component';
import { DataStorageService } from './../shared/data-storage.service';
import { RecipeService } from './../services/recipe.service';
import { ShoppingListService } from './../services/shopping-list.service';
import { SharedModule } from './../shared/shared.module';
import { AppRoutesModule } from './../app.routing.module';

@NgModule({
    declarations: [
        HeaderComponent
    ],
    imports: [
        AppRoutesModule,
        SharedModule
    ],
    exports: [
        HeaderComponent,
        AppRoutesModule
    ],
    providers: [
        ShoppingListService,
        RecipeService,
        DataStorageService
    ]
})
export class CoreModule {}
