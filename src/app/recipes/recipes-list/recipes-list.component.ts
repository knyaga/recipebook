import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Recipe } from './../recipe.model';
import { RecipeService } from './../../services/recipe.service';

@Component({
    selector: 'app-recipes-list',
    templateUrl: './recipes-list.component.html',
    styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {

    recipes: Recipe[];

    constructor(
        private recipeService: RecipeService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {

        this.recipeService.recipesChanged.subscribe(
           (recipes: Recipe[]) => {
               this.recipes = recipes;
           }
        );
        this.recipes = this.recipeService.getRecipes();
    }

    onNewClicked() {
      this.router.navigate(['create'], { relativeTo: this.route});
    }
}
