import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { RecipeService } from './../services/recipe.service';

@Injectable()
export class DataStorageService {

    baseUrl = 'https://recipebook-cdc3e.firebaseio.com';

    constructor(private _http: Http, private recipeService: RecipeService) {}

    saveRecipes() {
      return this._http.put(this.baseUrl + '/recipes.json', this.recipeService.getRecipes());
    }

    getRecipes() {

    }
}
