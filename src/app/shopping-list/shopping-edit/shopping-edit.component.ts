import { Component, ViewChild, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Ingredient } from './../../shared/ingredient.model';
import { ShoppingListService } from './../../services/shopping-list.service';

@Component({
    selector: 'app-shopping-edit',
    templateUrl: './shopping-edit.component.html',
    styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
    @ViewChild('shoppingListForm') slForm: NgForm;

    selectedIngredient: Ingredient;
    editMode = false;
    editedIndex: number;

    constructor(private shoppingListService: ShoppingListService) {}

    ngOnInit() {
        this.shoppingListService.startEditing.subscribe(
            (index: number) => {
                this.editMode = true;
                this.editedIndex = index;
                this.selectedIngredient = this.shoppingListService.getIngredientById(index);
                this.slForm.setValue({
                    name: this.selectedIngredient.name,
                    amount: this.selectedIngredient.amount
                });
            }
        );
    }

    onSubmitIngredient() {
        const name = this.slForm.value.name;
        const amount = this.slForm.value.amount;
        const newIngredient = new Ingredient(name, amount);

        if (this.editMode) {
            this.shoppingListService.updateIngredient(this.editedIndex, newIngredient);
        }else {
            this.shoppingListService.addIngredient(newIngredient);
        }
        this.editMode = false;
        this.slForm.reset();
    }

    onClearIngredient() {
        this.slForm.reset();
        this.editMode = false;
    }

    onDeleteIngredient() {
        this.onClearIngredient();
        this.shoppingListService.removeIngredient(this.editedIndex);

    }
}