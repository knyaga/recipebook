import { Component } from '@angular/core';

import { ShoppingListService } from './services/shopping-list.service';
import { RecipeService } from './services/recipe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RecipeService, ShoppingListService]
})
export class AppComponent {
  recipes = true;
  shopping = false;

  displayRecipes(recipesEvent) {
    this.recipes = recipesEvent.recipes;
    this.shopping = recipesEvent.shoppingList;
  }

  showShoppingList(shoppingEvent) {
    this.recipes = shoppingEvent.recipes;
    this.shopping = shoppingEvent.shoppingList;
  }
}
